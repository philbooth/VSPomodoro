// Copyright © 2010 Phil Booth
//
// This file is part of VSPomodoro.
//
// VSPomodoro is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// VSPomodoro is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VSPomodoro.  If not, see <http://www.gnu.org/licenses/>.

using EnvDTE;
using EnvDTE80;
using Extensibility;
using Microsoft.VisualStudio.CommandBars;
using PB;
using System;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Media;
using VSPomodoro.Properties;

namespace VSPomodoro
{
	public class Connect : IDTExtensibility2, IDTCommandTarget
	{
		public Connect()
		{
		}

		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
			app = (DTE2)application;
			addIn = (AddIn)addInInst;
			if(connectMode == ext_ConnectMode.ext_cm_UISetup)
				AddMenuItem();
		}

		public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
		{
		}

		public void OnAddInsUpdate(ref Array custom)
		{
		}

		public void OnStartupComplete(ref Array custom)
		{
		}

		public void OnBeginShutdown(ref Array custom)
		{
			renderer.Dispose();
			renderer = null;
		}

		public void QueryStatus(string commandName, vsCommandStatusTextWanted neededText, ref vsCommandStatus status, ref object commandText)
		{
			if(neededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone && commandName == commandId)
			{
				if(window.Visible)
					status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported;
				else
					status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
			}
		}

		public void Exec(string commandName, vsCommandExecOption executeOption, ref object varIn, ref object varOut, ref bool handled)
		{
			handled = false;
			if(executeOption == vsCommandExecOption.vsCommandExecOptionDoDefault && commandName == commandId)
			{
				CreateWindow();
				handled = true;
			}
		}

		private void AddMenuItem()
		{
			try
			{
				CommandBarPopup menuPopup = (CommandBarPopup)((Microsoft.VisualStudio.CommandBars.CommandBars)app.CommandBars)["MenuBar"].Controls[localisedMenuName];
				if(menuPopup == null)
					return;

				object[] contextGUIDS = new object[] { };

				Command command = ((Commands2)app.Commands).AddNamedCommand2(addIn, addinName, addinName, buttonTooltip, buttonIconIsFromMSOffice, buttonIconId, ref contextGUIDS, (int)vsCommandStatus.vsCommandStatusSupported + (int)vsCommandStatus.vsCommandStatusEnabled, (int)vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeButton);
				if(command == null)
					return;

				command.AddControl(menuPopup.CommandBar, 1);
			}
			catch(ArgumentException)
			{
				// A menu item with that name already exists
			}
		}

		private string localisedMenuName
		{
			get
			{
				try
				{
					ResourceManager resourceManager = new ResourceManager(resourceName, Assembly.GetExecutingAssembly());
					CultureInfo cultureInfo = new CultureInfo(app.LocaleID);
					return resourceManager.GetString(String.Concat(cultureInfo.TwoLetterISOLanguageName, menuName));
				}
				catch
				{
				}

				return menuName;
			}
		}

		private void CreateWindow()
		{
			object control = null;
			window = ((Windows2)app.Windows).CreateToolWindow2(addIn, controlLibraryPath, controlName, addinName, windowGUID, ref control);
			if(window.IsFloating)
			{
				window.Width = preferredWidth;
				window.Height = preferredHeight;
			}
			window.Visible = true;
			renderer = (HTMLRenderer)control;
			RenderView();
		}

		private static string controlLibraryPath
		{
			get { return addinDirectory + Path.DirectorySeparatorChar + controlLibraryName; }
		}

		private static string addinDirectory
		{
			get { return Path.GetDirectoryName(addinPath); }
		}

		private static string addinPath
		{
			get { return Uri.UnescapeDataString((new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path)); }
		}

		public static string addinName
		{
			get { return Assembly.GetExecutingAssembly().GetName().Name; }
		}

		private static string addinVersion
		{
			get { return GetAssemblyVersion(addinPath); }
		}

		private static string GetAssemblyVersion(string assemblyPath)
		{
			return GetAssemblyName(assemblyPath).Version.ToString();
		}

		private static AssemblyName GetAssemblyName(string assemblyPath)
		{
			return AssemblyName.GetAssemblyName(assemblyPath);
		}

		private void RenderView()
		{
			renderer.RenderMarkup(String.Format(Resources.VSPomodoro_HTML, "file:///" + addinDirectory + Path.DirectorySeparatorChar));
			renderer.SetCallback(new RendererBridge(this));
		}

		public class RendererBridge
		{
			public RendererBridge(Connect owner)
			{
				this.owner = owner;
			}

			public void Alarm(bool audio, bool visual, string message)
			{
				owner.InvokeAlarm(audio, visual, message);
			}

			Connect owner;
		}

		public void InvokeAlarm(bool audio, bool visual, string message)
		{
			window.Activate();
			// HACK: EnvDTE.Window.HWnd is always zero except for DTE.MainWindow, so we make do with that instead.
			Alarm.Invoke(new WindowArgument(app.MainWindow), audio, visual, message);
		}

		private readonly static string className = "VSPomodoro.Connect";
		private readonly static string commandId = className + "." + addinName;
		private readonly static string buttonTooltip = "Open " + addinName;
		private readonly static bool buttonIconIsFromMSOffice = false;
		private readonly static int buttonIconId = 100;
		private readonly static string resourceName = "VSPomodoro.CommandBar";
		private readonly static string menuName = "Tools";
		private readonly static string windowGUID = "{13DE119C-1E60-419b-9638-D3709C8D3484}";
		private readonly static string controlLibraryName = "PBCtrl.dll";
		private readonly static string controlName = "PB.HTMLRenderer";
		private readonly static int preferredWidth = 315;
		private readonly static int preferredHeight = 250;

		private DTE2 app;
		private AddIn addIn;
		private Window window;
		private HTMLRenderer renderer;
	}
}