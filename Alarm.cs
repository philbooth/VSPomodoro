// Copyright � 2010 Phil Booth
//
// This file is part of VSPomodoro.
//
// VSPomodoro is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// VSPomodoro is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VSPomodoro.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Media;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace VSPomodoro
{
	static class Alarm
	{
		public static void Invoke(WindowArgument window, bool audio, bool visual, string message)
		{
			if(audio)
				Beep();

			bool flashUntilUserAcknowledgement = message != null && message.Length > 0;

			if(visual)
				FlashWindow(window, !flashUntilUserAcknowledgement);

			if(flashUntilUserAcknowledgement)
			{
				NotifyUser(message, window);

				if(visual)
					StopFlashingWindow(window);
			}
		}

		private static void FlashWindow(WindowArgument window, bool flashIndefinitely)
		{
			FLASHWINDOWINFO info = CreateFlashWindowInfo(window, flashIndefinitely ? Commands.FlashBriefly : Commands.FlashUntilExplicitlyStopped);
			FlashWindowEx(ref info);
		}

		private static FLASHWINDOWINFO CreateFlashWindowInfo(WindowArgument window, Commands command)
		{
			FLASHWINDOWINFO info = new FLASHWINDOWINFO();

			info.cbSize = Convert.ToUInt32(Marshal.SizeOf(info));
			info.hwnd = window.Handle;
			info.dwFlags = (UInt32)command;
			info.uCount = command == Commands.FlashUntilExplicitlyStopped ? UInt32.MaxValue : 5;
			info.dwTimeout = 0;

			return info;
		}

		private enum Commands
		{
			StopFlashing = 0,
			FlashUntilExplicitlyStopped = 3,
			FlashBriefly = 7
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct FLASHWINDOWINFO
		{
			public UInt32 cbSize;
			public IntPtr hwnd;
			public UInt32 dwFlags;
			public UInt32 uCount;
			public UInt32 dwTimeout;
		}

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool FlashWindowEx(ref FLASHWINDOWINFO pwfi);

		private static void Beep()
		{
			SystemSounds.Asterisk.Play();
		}

		private static void NotifyUser(string message, WindowArgument alarmWindow)
		{
			MessageBox.Show(alarmWindow, message, Connect.addinName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
		}

		private static void StopFlashingWindow(WindowArgument window)
		{
			FLASHWINDOWINFO info = CreateFlashWindowInfo(window, Commands.StopFlashing);
			FlashWindowEx(ref info);
		}
	}
}
