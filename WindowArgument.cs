// Copyright � 2010 Phil Booth
//
// This file is part of VSPomodoro.
//
// VSPomodoro is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// VSPomodoro is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VSPomodoro.  If not, see <http://www.gnu.org/licenses/>.

using EnvDTE;
using System;
using System.Windows.Forms;

namespace VSPomodoro
{
	class WindowArgument : IWin32Window
	{
		public WindowArgument(Window window)
		{
			handle = new IntPtr(window.HWnd);
		}

		public IntPtr Handle
		{
			get { return handle; }
		}

		private IntPtr handle;
	}
}
