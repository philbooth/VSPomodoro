# VSPomodoro

This project is an integrated pomodoro timer for Visual Studio. If
you are unfamiliar with the idea of a pomodoro timer, you should
read up on [the Pomodoro Technique][1]. Basically it is a simple
time-management practice that involves dividing your day into a
series of 25-minute long, focused slices. If you are an habitual
procrastinator or have difficulty managing interruptions, you
should give the technique a try.

Using VSPomodoro should be fairly self-explanatory. After running
the installer, a VSPomodoro item will be added to the Tools menu in
Visual Studio. This menu item will open the VSPomodoro window, which
contains a countdown timer and some buttons labelled `Start`, `Stop`
and `Reset`, which do pretty much what you'd expect. The timer can
be easily set to countdown from any time you want, if you prefer to
follow a different routine. However there are only two digits each
availble for minutes and seconds, so you're out of luck if you divide
your time into chunks larger than 99'99".

[1]: http://www.pomodorotechnique.com/faq.html

